jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        if ($.exists(".mainMenu")) {
            $('.menu h2').on('click', function(){
                $(this).toggleClass('active').parent().next().stop().slideToggle('fast');
            });
        }

        if ($.exists(".docBox")) {
            lightbox.option({
                'alwaysShowNavOnTouchDevices': true,
                'showImageNumberLabel': false
            })
        }

        if ($.exists(".gallBox")) {
            lightbox.option({
                'alwaysShowNavOnTouchDevices': true,
                'showImageNumberLabel': false
            })
        }



        if($.exists(".open-pop-up")) {
            $('.open-pop-up').click(function (e) {
                e.preventDefault();
                var href = $(this).attr('href');


                if ($(href).hasClass('active-pop-up')) {
                    $('.pop-up-wrap').removeClass('active-pop-up');
                    $(href).removeClass('active-pop-up');
                    $('#parallax div').addClass('parallax');

                }else{
                    $('.pop-up-wrap').removeClass('active-pop-up');
                    $(href).addClass('active-pop-up');
                    $('#parallax div').removeClass('parallax');
                }


            });
        }



    });
}).call(this);


